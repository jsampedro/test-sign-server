<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 17/03/17
 * Time: 12:29
 */

namespace AppBundle\Service;

class PDFHelper
{
    public static function generateFiles($workPath)
    {
        $pdf = new \FPDI();
        $data = [];
        $pdfPath = $workPath.'/uploaded.pdf';
        $count = $pdf->setSourceFile($pdfPath);

        for ($i = 1; $i <= $count; $i++) {
            $tplidx = $pdf->ImportPage($i);
            $s = $pdf->getTemplatesize($tplidx);
            $imagePath = $workPath.'/image_'.$i.'.png';
            PDFHelper::getPageImage($pdfPath, $i)->writeImage($imagePath);
            $data[] = [
                'width' => $s['w'],
                'height' => $s['h'],
                'image' => '/generated/image_'.$i.'.png'
            ];
        }


        return $data;
    }

    /**
     * @param $pdfPath
     * @param $page
     * @return \Imagick
     */
    public static function getPageImage($pdfPath, $page)
    {
        $im = new \Imagick();
        $im->setResolution(200, 200);
        $im->readImage(sprintf('%s[%d]', $pdfPath, $page - 1));
        $im->setImageFormat('png');

        return $im;
    }
}
