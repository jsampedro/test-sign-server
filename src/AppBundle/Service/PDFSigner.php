<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 9/03/17
 * Time: 15:44
 */

namespace AppBundle\Service;


class PDFSigner
{
    static public function sign($pdfPath, $image, $page = 1, $x, $y, $width, $height)
    {

        $pdf = new \FPDI();

        $count = $pdf->setSourceFile($pdfPath);

        for ($i = 1; $i <= $count; $i++) {
            $tplidx = $pdf->ImportPage($i);
            $s = $pdf->getTemplatesize($tplidx);
            $pdf->AddPage('P', array($s['w'], $s['h']));
            $pdf->useTemplate($tplidx);
            if ($i == $page) {
                $pdf->Image($image, $x, $y, $width, $height);
            }
        }

        $pdf->Output($pdfPath.'_signed', 'F');

    }
}
