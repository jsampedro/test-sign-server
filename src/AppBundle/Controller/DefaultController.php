<?php

namespace AppBundle\Controller;

use AppBundle\Service\PDFHelper;
use AppBundle\Service\PDFSigner;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="generate")
     */
    public function indexAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('file', FileType::class)
            ->getForm();

        if ($form->handleRequest($request)->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('file')->getData();
            $workPath = $this->getParameter('kernel.project_dir').'/web/generated';
            $uploadedFile->move($workPath, 'uploaded.pdf');

            return $this->render(
                'default/generated.html.twig',
                [

                    'images' => PDFHelper::generateFiles(
                        $workPath
                    )
                ]
            );
        }

        return $this->render(
            'default/index.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/sign", name="sign")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function signAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('page', NumberType::class)
            ->add('x', NumberType::class)
            ->add('y', NumberType::class)
            ->add('width', NumberType::class)
            ->add('height', NumberType::class)
            ->add('image', TextareaType::class)
            ->getForm();


        if ($form->handleRequest($request)->isValid()) {
            $signatureImage = tempnam(sys_get_temp_dir(), md5(time().uniqid()));

            $this->base64_to_jpeg($form->get('image')->getData(), $signatureImage);
            PDFSigner::sign(
                $this->getParameter('kernel.project_dir').'/web/generated/uploaded.pdf',
                $signatureImage,
                $form->get('page')->getData(),
                $form->get('x')->getData(),
                $form->get('y')->getData(),
                $form->get('width')->getData(),
                $form->get('height')->getData()
            );

            return BinaryFileResponse::create(
                $this->getParameter('kernel.project_dir').'/web/generated/uploaded.pdf_signed'
            );
        }


        return $this->render(
            'default/sign.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        return $output_file;
    }
}
