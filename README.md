```composer install```

```bin/console server:run```

Copiar un archivo pdf a var/data/test.pdf

Ir a ```http://localhost:8000```

Mostrara los datos de cada pagina del pdf. Estos datos son los que debe tener la aplicacion cliente que va a generar la firma.
Por cada pagina se da una dimension y la ruta a la imagen.

Al ir a firmar se introduce la pagina (comenzando en 1, no en 0), la posicion X e Y, asi como la imagen en base64.

Si todo va bien se devuelve el pdf generado, que se crea en ```var\data\test.pdf_signed```